#!"D:\Applications\Python 3.4\python.exe"

import sys
import csv
import ast
import io
import re

CATEGORIES = {  
'Explosives'                : 'Chemicals',
'Hydrogen Fuel'             : 'Chemicals',
'Mineral Oil'               : 'Chemicals',
'Pesticides'                : 'Chemicals',
'Clothing'                  : 'Consumer Items',
'Consumer Technology'       : 'Consumer Items',
'Domestic Appliances'       : 'Consumer Items',
'Algae'                     : 'Foods',
'Animal Meat'               : 'Foods',
'Coffee'                    : 'Foods',
'Fish'                      : 'Foods',
'Food Cartridges'           : 'Foods',
'Fruit And Vegetables'      : 'Foods',
'Grain'                     : 'Foods',
'Synthetic Meat'            : 'Foods',
'Tea'                       : 'Foods',
'Polymers'                  : 'Industrial Materials',
'Semiconductors'            : 'Industrial Materials',
'Superconductors'           : 'Industrial Materials',
'Beer'                      : 'Legal Drugs',
'Liquor'                    : 'Legal Drugs',
'Narcotics'                 : 'Legal Drugs',
'Tobacco'                   : 'Legal Drugs',
'Wine'                      : 'Legal Drugs',
'Atmospheric Processors'    : 'Machinery',
'Crop Harvesters'           : 'Machinery',
'Marine Equipment'          : 'Machinery',
'Microbial Furnaces'        : 'Machinery',
'Mineral Extractors'        : 'Machinery',
'Power Generators'          : 'Machinery',
'Water Purifiers'           : 'Machinery',
'Agri-Medicines'            : 'Medicines',
'Basic Medicines'           : 'Medicines',
'Combat Stabilisers'        : 'Medicines',
'Performance Enhancers'     : 'Medicines',
'Progenitor Cells'          : 'Medicines',
'Aluminium'                 : 'Metals',
'Beryllium'                 : 'Metals',
'Cobalt'                    : 'Metals',
'Copper'                    : 'Metals',
'Gallium'                   : 'Metals',
'Gold'                      : 'Metals',
'Indium'                    : 'Metals',
'Lithium'                   : 'Metals',
'Palladium'                 : 'Metals',
'Platinum'                  : 'Metals',
'Silver'                    : 'Metals',
'Tantalum'                  : 'Metals',
'Titanium'                  : 'Metals',
'Uranium'                   : 'Metals',
'Bauxite'                   : 'Minerals',
'Bertrandite'               : 'Minerals',
'Coltan'                    : 'Minerals',
'Gallite'                   : 'Minerals',
'Indite'                    : 'Minerals',
'Lepidolite'                : 'Minerals',
'Rutile'                    : 'Minerals',
'Uraninite'                 : 'Minerals',
'Imperial Slaves'           : 'Slavery',
'Slaves'                    : 'Slavery',
'Advanced Catalysers'       : 'Technology',
'Animal Monitors'           : 'Technology',
'Aquaponic Systems'         : 'Technology',
'Auto-Fabricators'          : 'Technology',
'Bioreducing Lichen'        : 'Technology',
'Computer Components'       : 'Technology',
'H.E. Suits'                : 'Technology',
'Land Enrichment Systems'   : 'Technology',
'Resonating Separators'     : 'Technology',
'Robotics'                  : 'Technology',
'Leather'                   : 'Textiles',
'Natural Fabrics'           : 'Textiles',
'Synthetic Fabrics'         : 'Textiles',
'Biowaste'                  : 'Waste',
'Chemical Waste'            : 'Waste',
'Scrap'                     : 'Waste',
'Battle Weapons'            : 'Weapons',
'Non-Lethal Weapons'        : 'Weapons',
'Personal Weapons'          : 'Weapons',
'Reactive Armour'           : 'Weapons'
}

HEADER = """#! trade.py import -

# File syntax:
# <item name> <sell> <buy> [<demand> <stock> [<timestamp>]]
#   Use '?' for demand/stock when you don't know/care,
#   Use '-' for demand/stock to indicate unavailable,
#   Otherwise use a number followed by L, M or H, e.g.
#     1L, 23M or 30000H

#     Item Name                SellCr   BuyCr     Demand     Stock  Timestamp
"""

DATA = {}

def ingest_csv_rows(csvObj):
    "This ingests all the rows from CSV format"
    for row in csvObj:
        # We don't use DictReader because some headers are missing in EliteOCR output :(
        if row[0] == "System":
            continue

        System      = row[0].upper()
        System      = System.strip()
        if not System or System == '':
            continue
        Station     = row[1].upper()
        Station     = Station.strip()
        if not Station or Station == '':
            continue
        SysSta      = System + "/" + Station
        Comm        = row[2]
        Comm        = Comm.strip()
        if not Comm in CATEGORIES:
            continue
        Cat         = CATEGORIES[Comm]
        Sell        = row[3]
        Buy         = row[4] or '0'
        DemandNum   = row[5]
        DemandChar  = row[6]
        if DemandNum == '' and Buy != '0':
            DemandChar = '-'
        else:
            DemandChar  = DemandChar[:1] or '?'
        Demand      = DemandNum + DemandChar
        StockNum    = row[7]
        StockChar   = row[8]
        if StockNum == '' and Buy == '0':
            StockChar = '-'
        else:
            StockChar   = StockChar[:1] or '?'
        Stock       = StockNum + StockChar
        Timestamp   = row[9]
        Timestamp   = Timestamp[0:10] + " " + Timestamp[11:19]
        
        if not SysSta in DATA:
            DATA[SysSta] = {}
        if not Cat in DATA[SysSta]:
            DATA[SysSta][Cat] = {}

        DATA[SysSta][Cat][Comm] = [ Sell, Buy, Demand, Stock, Timestamp ]
        
    return

def ingest_ast_rows(file):
    "This ingests all the rows from AST format"
    for line in file:
        line        = re.sub(r'\t', r'', line) 
        # pattern    = re.compile(r'""([^"]*)""')
        # for mo in re.findall(pattern, line):
            # newStr = re.sub(r"'", r"\'", mo)
            # newStr = "'" + newStr + "'"
            # oldStr = '""' + mo + '""'
            # line = re.sub(oldStr, newStr, line)
        line        = re.sub(r'"', r"'", line)
        line        = re.sub(r"(\w)\'(?![,'|:}])", r"\1\'", line)
        line        = re.sub(r'"+', r"'", line)
        try:
            astObj  = ast.literal_eval(line)
        except:
            sys.stderr.write("[ERROR] Bad line found: " + line)
            continue
        msg         = astObj['message']
        System      = msg['systemName'].upper()
        System      = System.strip()
        if not System or System == '':
            continue
        Station     = msg['stationName'].upper()
        Station     = Station.strip()
        if not Station or Station == '':
            continue
        SysSta      = System + "/" + Station
        Comm        = msg['itemName']
        Comm        = Comm.strip()
        if not Comm in CATEGORIES:
            continue
        Cat         = CATEGORIES[Comm]
        Sell        = msg['sellPrice']
        Buy         = msg['buyPrice'] or '0'
        DemandNum   = msg['demand']
        Demand      = ''
        if DemandNum == 0:
            Demand = '-'
        else:
            Demand = str(DemandNum) + '?'
        StockNum    = msg['stationStock']
        Stock       = ''
        if StockNum == 0:
            Stock = '-'
        else:
            Stock = str(StockNum) + '?'
        Timestamp   = msg['timestamp']
        Timestamp   = Timestamp.strip()
        Timestamp   = Timestamp[0:10] + " " + Timestamp[11:19]
        
        if not SysSta in DATA:
            DATA[SysSta] = {}
        if not Cat in DATA[SysSta]:
            DATA[SysSta][Cat] = {}
        if not Comm in DATA[SysSta]:
            DATA[SysSta][Cat][Comm] = {}

        if len(DATA[SysSta][Cat][Comm]) > 4:
            if Timestamp > DATA[SysSta][Cat][Comm][4]:
                DATA[SysSta][Cat][Comm] = [ Sell, Buy, Demand, Stock, Timestamp ]
            else:
                sys.stderr.write("[OLD DATA] {0} - {1} :: {2} > {3}".format(SysSta, Comm, Timestamp, DATA[SysSta][Cat][Comm][4]))
                continue

        DATA[SysSta][Cat][Comm] = [ Sell, Buy, Demand, Stock, Timestamp ]
        
    return

# Main program
if len(sys.argv) < 2:
    text = sys.stdin.read()
else:
    with open(sys.argv[1], 'r') as inFile:
        text = inFile.read()

myFile = io.StringIO(text)
c = myFile.read(1)
myFile.seek(0)
if c != '{':
    ingest = csv.reader(myFile, delimiter=';')
    ingest_csv_rows(ingest)
else:
    ingest_ast_rows(myFile)

print (HEADER)
for a in sorted(DATA):
    print ("")
    print ("@", a)
    for b in sorted(DATA[a]):
        print ("   +", b)
        for c in sorted(DATA[a][b]):
            width=5
            print("      {0:23}{1:>8}{2:>8}{3:>11}{4:>10}  {5}".format(c,
            DATA[a][b][c][0],
            DATA[a][b][c][1],
            DATA[a][b][c][2],
            DATA[a][b][c][3],
            DATA[a][b][c][4]))

# The end.