#!"D:\Applications\Python 3.4\python.exe"

import sys
import json
import csv
import ast
import re
import pickle
import requests
import argparse
import datetime
from pyquery import PyQuery as pq
from lxml.cssselect import etree
from requests.exceptions import HTTPError

PASSWD                  = ".passwd"
COOKIE                  = ".cookies"
USER_AGENT              = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2248.0 Safari/537.36"

ETT_HOME_PAGE           = "http://www.elitetradingtool.co.uk"
ETT_LOGIN_PAGE          = ETT_HOME_PAGE  + "/Account/Login"
ETT_ADMIN_PAGE          = ETT_HOME_PAGE  + "/Admin"
ETT_SELECTOR_QUERY      = ETT_ADMIN_PAGE + "/SelectorData?query="
ETT_SYSTEM_PAGE         = ETT_ADMIN_PAGE + "/System?id="
ETT_STATIONS_PAGE       = ETT_ADMIN_PAGE + "/SystemStations?id="
ETT_COMMODITIES_PAGE    = ETT_ADMIN_PAGE + "/StationCommodities?categoryId=0&id="

CSV_HEADERS             = "System;Station;Commodity;Sell;Buy;Demand;;Supply;;Date;"

parser = argparse.ArgumentParser(description='This script will download information from Thrudd\'s Elite Trading Tool website.')
parser.add_argument('type', type=str, choices=['system', 'station'], help='Specify system or station')
parser.add_argument('name', type=str, help='System/station to query')
args = parser.parse_args()

def loadCookies(filename):
    with open(filename, 'w+b') as fh:
        try:
            pickleLoad = pickle.load(fh)
        except:
            return
        else:
            return pickleLoad
    
def saveCookies(cookieJar, filename):
    with open(filename, 'w+b') as fh:
        pickle.dump(cookieJar, fh)

def myHttpHandler(url, data={}):
    try:
        if len(data) == 0:
            resp = sess.get(url)
        else:
            resp = sess.post(url, data=data)
    except HTTPError as e:
        sys.stderr.write ("[ERROR] The server couldn't fulfill the request.")
        sys.stderr.write ("[ERROR] Error code: ", e.code)
        return False
    else:
        return resp
    return False

def logged_in():
    response = myHttpHandler(ETT_ADMIN_PAGE)
    if response:
        page = pq(response.text)
        
    if len(page.find('#ContentContainer').find('section#loginForm')) == 0:
        x = True
    else:
        x = False
    return x
    
def do_login():
    with open (PASSWD, 'r') as fh:
        inFile = fh.read()
    try:
        astObj = ast.literal_eval(inFile)
    except:
        sys.stderr.write("[ERROR] Bad format for .passwd file.")
        return False
        
    # Now actually log in.
    response = myHttpHandler(ETT_LOGIN_PAGE, astObj)

    return True

def get_station_id(name):
    response = myHttpHandler(ETT_SELECTOR_QUERY + name)
    myJson = json.loads(response.text)
    id = False
    for set in myJson:
        if name.lower() == set['Station'].lower():
            id = set['StationId']
            break
    return id

def get_system_id(name):
    response = myHttpHandler(ETT_SELECTOR_QUERY + name)
    myJson = json.loads(response.text)
    id = False
    for set in myJson:
        if name.lower() == set['System'].lower():
            id = set['SystemId']
            break
    return id

def get_commodities(id):
    response = myHttpHandler(ETT_COMMODITIES_PAGE + str(id))
    myJson = json.loads(response.text)
    return myJson

def get_stations_for_system(id):
    response = myHttpHandler(ETT_STATIONS_PAGE + str(id))
    myJson = json.loads(response.text)
    return myJson['Stations']

def json_to_csv(myJson):
    print (CSV_HEADERS)
    fullName = myJson['StationName']
    systemName = re.sub(r'([^(]+)\s\([^)]+\)', r'\1', fullName)
    stationName = re.sub(r'[^(]+\s\(([^)]+)\)', r'\1', fullName)
    commodities = myJson['StationCommodities']
    for item in commodities:
        timestamp = re.sub(r'\/Date\((\d+)\)\/', r'\1', item['LastUpdate'])
        timestamp = datetime.datetime.utcfromtimestamp(int(int(timestamp)/1000))
        print ("{0};{1};{2};{3};{4};;;;;{5};".format(
            systemName,
            stationName,
            item['CommodityName'],
            item['Sell'],
            item['Buy'],
            timestamp))
    return

# Setup the session
sess = requests.Session()
sess.headers.update({ 'User-Agent': USER_AGENT })

# Load cookies from file into our session
myCookies = loadCookies(COOKIE)
if myCookies:
    sess.cookies = myCookies

if args.type == 'system':
    if not logged_in():
        sys.stderr.write ("You are not logged in.  Attempting to log you in...\n")
        do_login()
        if not logged_in():
            sys.stderr.write ("You are still not logged in.  Quitting.\n")
            exit()
        else:
            sys.stderr.write ("Login successful!\n")
    systemId = get_system_id(args.name)
    if systemId:
        sys.stderr.write ("Found system -- id: " + str(systemId) + "\n")
        # Here's where you have to do real logic
        for station in get_stations_for_system(systemId):
            sys.stderr.write ("Found station -- id: " + str(station['Id']) + " -- Name: " + station['Name'] + "\n")
            json_to_csv(get_commodities(station['Id']))
    else:
        sys.stderr.write ("System not found on ETT.\n")
elif args.type == 'station':
    if not logged_in():
        sys.stderr.write ("You are not logged in.  Attempting to log you in...\n")
        do_login()
        if not logged_in():
            sys.stderr.write ("You are still not logged in.  Quitting.\n")
            exit()
        else:
            sys.stderr.write ("Login successful!\n")
    stationId = get_station_id(args.name)
    if stationId:
        sys.stderr.write ("Station found with ID = " + str(stationId) + "\n")
        json_to_csv(get_commodities(stationId))
    else:
        sys.stderr.write ("Station not found on ETT.\n")
else:
    sys.stderr.write ("How did you get here? Argparse should have prevented this!\n")

    if logged_in():
        sys.stderr.write ("[DEBUG] You are already logged in.\n")
    else:
        sys.stderr.write ("[DEBUG] You are not logged in.  Attempting to log you in...\n")
        do_login()
        if logged_in():
            sys.stderr.write ("[DEBUG] You are now logged in.\n")
        else:
            sys.stderr.write ("[DEBUG] You are still not logged in.  :(\n")

# Write cookies to file
saveCookies(sess.cookies, COOKIE)


