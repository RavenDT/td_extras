#!"D:\Applications\Python 3.4\python.exe"

import sys
import csv
import argparse

parser = argparse.ArgumentParser(description='This script will intelligently merge new system or station data into an existing database.')
parser.add_argument('type', type=str, choices=['system', 'station'], help='Specify system or station')
parser.add_argument('infile', type=argparse.FileType(mode='r', encoding='UTF-8'), help='New data to ingest')
parser.add_argument('outfile', type=argparse.FileType(mode='r+', encoding='UTF-8'), help='Preexisting database')
args = parser.parse_args()

SystemHeader = "unq:name,pos_x,pos_y,pos_z,name@Added.added_id,modified"
StationHeader = "unq:name@System.system_id,unq:name,ls_from_star,blackmarket,max_pad_size"

def ingest_systems(csvObj):
    "This ingests systems into DATA"
    for row in csvObj:
        if row[0] == 'unq:name':
            continue
        System  = row[0]
        PosX    = row[1]
        PosY    = row[2]
        PosZ    = row[3]
        Ver     = row[4]
        Date    = row[5]

        if not System in DATA:
            DATA[System] = {    'PosX' : PosX,
                                'PosY' : PosY,
                                'PosZ' : PosZ,
                                'Ver'  : Ver,
                                'Date' : Date }
            if SecondFile:
                print ("[FOUND] New System - {0} :: X={1}, Y={2}, Z={3}, Ver={4}, Date={5}".format(System,PosX,PosY,PosZ,Ver,Date))
        elif Date > DATA[System]['Date']:
            print ("[UPDATE] {0} :: X={1}, Y={2}, Z={3}, Ver={4}, Date={5} -> X={6}, Y={7}, Z={8}, Ver={9}, Date={10}".format(
                System,PosX,PosY,PosZ,Ver,Date,
                DATA[System]['PosX'],
                DATA[System]['PosY'],
                DATA[System]['PosZ'],
                DATA[System]['Ver'],
                DATA[System]['Date']))
            DATA[System] = {    'PosX' : PosX,
                                'PosY' : PosY,
                                'PosZ' : PosZ,
                                'Ver'  : Ver,
                                'Date' : Date }


    return

def ingest_stations(csvObj):
    "This ingests stations into DATA"
    for row in csvObj:
        if row[0] == 'unq:name@System.system_id':
            continue
        System  = row[0]
        Station = row[1]
        LSec    = row[2]
        BMark   = row[3]
        Pad     = row[4]

        if System in DATA:
            for x in DATA[System]:
                if System.upper() == x.upper():
                    System = x
                    if LSec != '0' and DATA[System][Station]['LSec'] == '0':
                        print ("[UPDATE] {0}/{1} :: LSec={2} -> LSec={3}".format(System, Station, LSec, DATA[System][Station]['LSec']))
                        DATA[System][Station]['LSec'] = LSec
                    
                    if BMark != "'?'" and DATA[System][Station]['BMark'] == "'?'":
                        print ("[UPDATE] {0}/{1} :: BMark={2} -> BMark={3}".format(System, Station, BMark, DATA[System][Station]['BMark']))
                        DATA[System][Station]['BMark'] = BMark

                    if Pad != "'?'" and DATA[System][Station]['Pad'] == "'?'":
                        print ("[UPDATE] {0}/{1} :: Pad={2} -> Pad={3}".format(System, Station, Pad, DATA[System][Station]['Pad']))
                        DATA[System][Station]['Pad'] = Pad
        if not System in DATA:
            DATA[System] = {}
 
        if not Station in DATA[System]:
            DATA[System][Station] = {   'LSec'  : LSec,
                                        'BMark' : BMark,
                                        'Pad'   : Pad }
            if SecondFile:
                print ("[FOUND] New Station - {0}/{1} :: LS={2}, BM={3}, Pad={4}".format(System,Station,LSec,BMark,Pad))

    return

DATA = {}
SecondFile = False

if args.type == 'system':
    csvIn1 = csv.reader(args.outfile, delimiter=',')
    csvIn2 = csv.reader(args.infile, delimiter=',')
    ingest_systems(csvIn1)
    SecondFile = True
    ingest_systems(csvIn2)
    # Now that everything is ingested, let's overwrite the existing database.
    args.outfile.seek(0)
    
    args.outfile.write(SystemHeader + "\n")
    for System in sorted(DATA):
        args.outfile.write("{0},{1},{2},{3},{4},{5}\n".format(
            System,
            DATA[System]['PosX'],
            DATA[System]['PosY'],
            DATA[System]['PosZ'],
            DATA[System]['Ver'],
            DATA[System]['Date']))

elif args.type == 'station':
    csvIn1 = csv.reader(args.outfile, delimiter=',')
    csvIn2 = csv.reader(args.infile, delimiter=',')
    ingest_stations(csvIn1)
    SecondFile = True
    ingest_stations(csvIn2)
    # Now that everything is ingested, let's overwrite the existing database.
    args.outfile.seek(0)
    
    args.outfile.write(StationHeader + "\n")
    for System in sorted(DATA):
        for Station in sorted(DATA[System]):
            args.outfile.write("{0},{1},{2},{3},{4}\n".format(
                System,
                Station,
                DATA[System][Station]['LSec'],
                DATA[System][Station]['BMark'],
                DATA[System][Station]['Pad']))

else:
    print ("How did you get here? Argparse should have prevented this!")

args.outfile.closed
args.infile.closed

# The end.