# TradeDangerous Extras #
#### Note: These are the first python scripts I've ever written. Be nice. ####

### List of scripts ###
* td_convert.py - Converts EliteOCR CSV format to TradeDangerous format for import using trade.py (TD)
* td_import.py - "Intelligently" imports new Station and Ship data into your existing data folder (TD)
* Client.py - From EDDN.  No modifications yet, but there might be some in the future.
* ett_download.py - Downloads data from Thrudd's Elite Trading Tool web site and parses into EliteOCR CSV format.

### How to run scripts ###
#### td_convert.py ####
* `td_convert.py [infile] [outfile]`
* `cat infile | td_convert.py > outfile`
#### td_import.py ####
* `td_import.py <system|station> <infile> <outfile>`
#### Client.py ####
* `Client.py > EDDN.log`
* Ctrl+C when you want to stop the listener.  I am thinking of making this better.
#### ett_download.py ###
* `ett_download.py <system|station> <systemName|stationName>`
* Example: `ett_download.py station "Aulin Enterprise" | td_convert.py > AulinEnterprise.prices`