:: Use 1 or 3 backslashes to escape double quotes

rem French Translation
sed -i ^
-e "s/'Explosifs'/'Explosives'/ig" ^
-e "s/u\"Car[8b]urant \\\\xc0 Base D'Hydrog\\\\xe8ne\"/'Hydrogen Fuel'/ig" ^
-e "s/u'Huile Min\\\\xe9rale'/'Mineral Oil'/ig" ^
-e "s/u'V\\\\xeatements'/'Clothing'/ig" ^
-e "s/u'\\\\xc9quipement De Loisir'/'Domestic Appliances'/ig" ^
-e "s/'Algue'/'Algae'/ig" ^
-e "s/'Viande'/'Animal Meat'/ig" ^
-e "s/u'Caf\\\\xe9'/'Coffee'/ig" ^
-e "s/'Poisson'/'Fish'/ig" ^
-e "s/'Cartouche Alimentaire'/'Food Cartridges'/ig" ^
-e "s/u'Fruits Et L\\\\xe9gumes'/'Fruit And Vegetables'/ig" ^
-e "s/u'C\\\\xe9r\\\\xe9ales'/'Grain'/ig" ^
-e "s/u'Viande Synth\\\\xe9tique'/'Synthetic Meat'/ig" ^
-e "s/'Th\\\\xe9'/'Tea'/ig" ^
-e "s/u'Polym\\\\xe8res'/'Polymers'/ig" ^
-e "s/'Semi-conducteurs'/'Semiconductors'/ig" ^
-e "s/'Supraconducteurs'/'Superconductors'/ig" ^
-e "s/u'Bi\\\\xe8re'/'Beer'/ig" ^
-e "s/'Spiritueux'/'Liquor'/ig" ^
-e "s/'Narcotiques'/'Narcotics'/ig" ^
-e "s/'Tabac'/'Tobacco'/ig" ^
-e "s/'Vin'/'Wine'/ig" ^
-e "s/u'Processeurs Atmosph\\\\xe9riques'/'Atmospheric Processors'/ig" ^
-e "s/'Moissoneuses'/'Crop Harvesters'/ig" ^
-e "s/u'\\\\xc9quipement Aquamarin'/'Marine Equipment'/ig" ^
-e "s/'Hauts Fourneaux Microbiens'/'Microbial Furnaces'/ig" ^
-e "s/'Extracteurs De Minerai'/'Mineral Extractors'/ig" ^
-e "s/u'G\\\\xe9n\\\\xe9rateurs'/'Power Generators'/ig" ^
-e "s/\"Purificateur D'eau\"/'Water Purifiers'/ig" ^
-e "s/u'Agri-m\\\\xe9dicaments'/'Agri-Medicines'/ig" ^
-e "s/u'M\\\\xe9dicaments Simples'/'Basic Medicines'/ig" ^
-e "s/'Stabilisateur De Combat'/'Combat Stabilisers'/ig" ^
-e "s/'Produits Dopants'/'Performance Enhancers'/ig" ^
-e "s/'Cellules Souches'/'Progenitor Cells'/ig" ^
-e "s/u'B\\\\xe9ryllium'/'Beryllium'/ig" ^
-e "s/'Cuivre'/'Copper'/ig" ^
-e "s/'Or'/'Gold'/ig" ^
-e "s/'Platine'/'Platinum'/ig" ^
-e "s/'Argent'/'Silver'/ig" ^
-e "s/'Tantale'/'Tantalum'/ig" ^
-e "s/'Titane'/'Titanium'/ig" ^
-e "s/u'L\\\\xe9pidolite'/'Lepidolite'/ig" ^
-e "s/u'Esclaves Imp\\\\xe9riaux'/'Imperial Slaves'/ig" ^
-e "s/u'Esclaves'/'Slaves'/ig" ^
-e "s/'Catalyseurs Complexes'/'Advanced Catalysers'/ig" ^
-e "s/u'Syst\\\\xe8mes De Surveillance'/'Animal Monitors'/ig" ^
-e "s/u'Syst\\\\xe8mes Aquaponiques'/'Aquaponic Systems'/ig" ^
-e "s/\"Dispositifs D'Autofabrication\"/'Auto-Fabricators'/ig" ^
-e "s/u'Lichen Bior\\\\xe9ducteur'/'Bioreducing Lichen'/ig" ^
-e "s/u\"Syst\\\\xe8mes D'Enrichissement Des Sols\"/'Land Enrichment Systems'/ig" ^
-e "s/u\"Syst\\\\xe8mes D'Enrichissement Des\"/'Land Enrichment Systems'/ig" ^
-e "s/u'S\\\\xe9parateurs \\\\xc0 R\\\\xe9sonance'/'Resonating Separators'/ig" ^
-e "s/'Robots'/'Robotics'/ig" ^
-e "s/'Cuir'/'Leather'/ig" ^
-e "s/'Fibre Textile Naturelle'/'Natural Fabrics'/ig" ^
-e "s/u'Tissu Synth\\\\xe9tique'/'Synthetic Fabrics'/ig" ^
-e "s/u'D\\\\x[0-9a-f][0-9a-f]chets Organiques'/'Biowaste'/ig" ^
-e "s/u'Mat\\\\xe9riaux Radioactifs'/'Chemical Waste'/ig" ^
-e "s/'Ferraille'/'Scrap'/ig" ^
-e "s/'Armes Militaires'/'Battle Weapons'/ig" ^
-e "s/'Armes Incapacitantes'/'Non-Lethal Weapons'/ig" ^
-e "s/'Armes De Poing'/'Personal Weapons'/ig" ^
-e "s/u'Protection R\\\\xe9active'/'Reactive Armour'/ig" ^
%*

:: After Clothing
:: -e "s/'???'/'Consumer Technology'/ig" ^
:: Before Domestic Appliances

:: After Bioreducing Lichen
:: -e "s/'???'/'Computer Components'/ig" ^
:: -e "s/'???'/'H.E. Suits'/ig" ^
:: Before Land Enrichment Systems   


rem German Translation
sed -i ^
-e "s/'Sprengstoffe'/'Explosives'/ig" ^
-e "s/'Wasserstoff-Treibstoff'/'Hydrogen Fuel'/ig" ^
-e "s/u'Mineral\\\\xf6l'/'Mineral Oil'/ig" ^
-e "s/'Pestizide'/'Pesticides'/ig" ^
-e "s/'Kleidung'/'Clothing'/ig" ^
-e "s/'Unterhaltungselektronik'/'Consumer Technology'/ig" ^
-e "s/u'Haushaltsger\\\\xe4te'/'Domestic Appliances'/ig" ^
-e "s/'Algen'/'Algae'/ig" ^
-e "s/'Tierfleisch'/'Animal Meat'/ig" ^
-e "s/'Kaffee'/'Coffee'/ig" ^
-e "s/'Fisch'/'Fish'/ig" ^
-e "s/'Nahrungskartuschen'/'Food Cartridges'/ig" ^
-e "s/u'Obst Un[od0] Gem\\\\xfcse'/'Fruit And Vegetables'/ig" ^
-e "s/'Getreide'/'Grain'/ig" ^
-e "s/u'K\\\\xfcnstliches Fleisch'/'Synthetic Meat'/ig" ^
-e "s/'Tee'/'Tea'/ig" ^
-e "s/'Polymere'/'Polymers'/ig" ^
-e "s/'Halbleiter'/'Semiconductors'/ig" ^
-e "s/'Supraleiter'/'Superconductors'/ig" ^
-e "s/'Bier'/'Beer'/ig" ^
-e "s/'Spirituosen'/'Liquor'/ig" ^
-e "s/'Drogen'/'Narcotics'/ig" ^
-e "s/'Tabak'/'Tobacco'/ig" ^
-e "s/'Wein'/'Wine'/ig" ^
-e "s/u'Atmosph\\\\xe4renprozessoren'/'Atmospheric Processors'/ig" ^
-e "s/'Agraryedimmente'/'Crop Harvesters'/ig" ^
-e "s/'Maritimausstattung'/'Marine Equipment'/ig" ^
-e "s/u'Mikrobielle \\\\xd6fen'/'Microbial Furnaces'/ig" ^
-e "s/'Mineralextraktoren'/'Mineral Extractors'/ig" ^
-e "s/'Stromerzeuger'/'Power Generators'/ig" ^
-e "s/'Wasserreiniger'/'Water Purifiers'/ig" ^
-e "s/'Agrar-Medikamente'/'Agri-Medicines'/ig" ^
-e "s/'Allgemeine Medikamente'/'Basic Medicines'/ig" ^
-e "s/'Kampfstabilisatoren'/'Combat Stabilisers'/ig" ^
-e "s/'Leistungssteigerer'/'Performance Enhancers'/ig" ^
-e "s/u'Vorl\?\\\\x[0-9a-f][0-9a-f]uferzellen'/'Progenitor Cells'/ig" ^
-e "s/'Vormuferzellen'/'Progentior Cells'/ig" ^
-e "s/'Kupfer'/'Copper'/ig" ^
-e "s/'Silber'/'Silver'/ig" ^
-e "s/'Platin'/'Platinum'/ig" ^
-e "s/'Tantal'/'Tantalum'/ig" ^
-e "s/'Titan'/'Titanium'/ig" ^
-e "s/'Uran'/'Uranium'/ig" ^
-e "s/'Bauxit'/'Bauxite'/ig" ^
-e "s/'Bertrandit'/'Bertrandite'/ig" ^
-e "s/'Gallit'/'Gallite'/ig" ^
-e "s/'Indit'/'Indite'/ig" ^
-e "s/'Lepidolith'/'Lepidolite'/ig" ^
-e "s/'Rutil'/'Rutile'/ig" ^
-e "s/'Uraninit'/'Uraninite'/ig" ^
-e "s/'Imperiale Sklaven'/'Imperial Slaves'/ig" ^
-e "s/'Sklaven'/'Slaves'/ig" ^
-e "s/'Fortschr. Katalysatoren'/'Advanced Catalysers'/ig" ^
-e "s/u'Tier\\\\xfcberwachung'/'Animal Monitors'/ig" ^
-e "s/'Aquaponiksyieme'/'Aquaponic Systems'/ig" ^
-e "s/'Fabrikatoren'/'Auto-Fabricators'/ig" ^
-e "s/'Bioreduzierende Flechten'/'Bioreducing Lichen'/ig" ^
-e "s/'Computerteile'/'Computer Components'/ig" ^
-e "s/u'Schutzanz\\\\xfcge'/'H.E. Suits'/ig" ^
-e "s/'Landanreicherungssysteme'/'Land Enrichment Systems'/ig" ^
-e "s/'Resonanzabgrenzer'/'Resonating Separators'/ig" ^
-e "s/'Roboter'/'Robotics'/ig" ^
-e "s/'Leder'/'Leather'/ig" ^
-e "s/'Naturfasern'/'Natural Fabrics'/ig" ^
-e "s/'Chemiefasern'/'Synthetic Fabrics'/ig" ^
-e "s/u'Biom\\\\xfcll'/'Biowaste'/ig" ^
-e "s/u'Chemiem\\\\xfcll'/'Chemical Waste'/ig" ^
-e "s/'Schrott'/'Scrap'/ig" ^
-e "s/'Kriegswaffen'/'Battle Weapons'/ig" ^
-e "s/'Nichtletale Waffen'/'Non-Lethal Weapons'/ig" ^
-e "s/u'Pers\\\\xf6nliche Waffen'/'Personal Weapons'/ig" ^
-e "s/u'Reaktivr\\\\xfcstung'/'Reactive Armour'/ig" ^
%*

@echo.